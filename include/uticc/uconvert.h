/**
 * @file uconvert.h
 * @author Stanislav Prokop
 * @note zlib License / Simplified BSD License
 */

#ifndef UCONVERT_H
#define	UCONVERT_H

// for std::string obviously
#include <string>
// for std::istringstream, std::ostringstream
#include <sstream>
// for std::bad_cast
#include <typeinfo>
// for LON_LONG_MAX
#include <climits>
// for numeric_limits
#include <limits>
// for conversion functions
#include <cstdlib>
// for errno
#include <cerrno>

namespace uticc {

struct BadConvert : public std::bad_cast {
};

///@cond DETAIL
namespace detail {

template <typename ReturnType, ReturnType (*standardIntegerConvertFunction)(const char*, char**, int)>
inline long convertUsingStandardFunction(const char* string) {
    char* p;
    // errno might be set already
    errno = 0;
    long result = std::strtol(string, &p, 0);
    int error = errno;
    errno = 0;
    /// error during conversion
    if (error != 0 || *p != '\0' || p == string) {
        throw BadConvert();
    }
    return result;
}
    
template <typename Type, bool IS_SPECIALIZED, bool IS_INTEGER, bool IS_SIGNED>
struct SpecializedConvertor {
    static Type convert(const char* string);
};

/// long double, double or float
template <typename Type>
struct SpecializedConvertor<Type, true, false, true> {
    static Type convert(const char* string) {
        char* p;
        // clean others mess
        errno = 0;
        double result = std::strtod(string, &p);
        int error = errno;
        errno = 0;
        /// error during conversion
        if (error != 0 || *p != '\0' || p == string) {
            throw BadConvert();
        }
        return (Type)result;
    }
};

/// signed integer
template <typename Type>
struct SpecializedConvertor<Type, true, true, true> {
    static Type convert(const char* string) {
        long result = convertUsingStandardFunction<long, std::strtol>(string);
        /// lower or higher than type limits
        if (result < std::numeric_limits<Type>::min() || result > std::numeric_limits<Type>::max()) {
            throw BadConvert();
        }
        return (Type)result;
    }
};

/// signed long
template <>
struct SpecializedConvertor<long, true, true, true> {
    static long convert(const char* string) {
        return convertUsingStandardFunction<long, std::strtol>(string);
    }
};

/// unsigned integer
template <typename Type>
struct SpecializedConvertor<Type, true, true, false> {
    static Type convert(const char* string) {
        unsigned long result = convertUsingStandardFunction<unsigned long, std::strtoul>(string);
        /// lower or higher than type limits
        if (result < 0 || result > std::numeric_limits<Type>::max()) {
            throw BadConvert();
        }
        return (Type)result;
    }
};

/// unsigned long
template <>
struct SpecializedConvertor<unsigned long, true, true, false> {
    static unsigned long convert(const char* string) {
        unsigned long result = convertUsingStandardFunction<unsigned long, std::strtoul>(string);
        /// higher than long max, might be negative
        if (result > (unsigned)std::numeric_limits<long>::max()) {
            long negativeResult = std::strtol(string, 0, 0);
            if (negativeResult < 0) {
                throw BadConvert();
            }
        }
        return result;
    }
};
///@endcond
/// unknown custom class
template <typename Type>
struct SpecializedConvertor<Type, false, false, false>;

/**
 * Template class used for conversion.
 * @tparam Source primitive type is expected
 */
template <typename Source>
struct Convert {
    inline operator std::string() const {
        std::ostringstream stream;
        // COMPILER ERROR?
        // maybe you are trying to convert something strange to string, pointer
        // or an unknown custom class
        stream << source;
        if (stream.fail()) {
            throw BadConvert();
        }
        return stream.str();
    }
    Source source;
};

/**
 * Template class specialization for const char*. Function std::sscanf is used
 * for conversion.
 */
template <>
struct Convert<const char*> {
    Convert(const char* source) : source(source) {
        if (source == NULL) {
            throw BadConvert();
        }
    }
//#if defined(LONG_LONG_MAX) || defined(LONLONG_MAX)
//    inline operator long long() const {
//        
//    }
//    
//    inline operator unsigned long long() const {
//        
//    }
//#endif// #ifdef LONG_LONG_MAX
    
    template <typename Type>
    operator Type() const {
        return SpecializedConvertor<Type,
                                    std::numeric_limits<Type>::is_specialized,
                                    std::numeric_limits<Type>::is_integer,
                                    std::numeric_limits<Type>::is_signed>
                ::convert(source);
    }

    const char* source;
};

/**
 * Template class specialization for std::string.
 */
template <>
struct Convert<std::string> {
    Convert(const std::string& source) : source(source) {}

    /**
     * @note
     * Conversion from std::string to primitive types is done by std::scanf, not
     * std::stringstream. Code for const char* is than simply reused. And maybe
     * speed is a little bit better, who knows...
     * @return 
     */
    template <typename Target>
    inline operator Target() const {
        
        return Convert<const char*>(source.c_str());
    }

    inline operator std::string() const {
        return source;
    }
    
    inline operator const char*() const {
        return source.c_str();
    }

    std::string source;
};

}//namespace detail

/**
 * This function converts from Source type to Target type.
 * 
 * How does it work? There is no magic around, for Target deduction is used
 * conversion operator. This function just wraps following code:
 * @code
 * // create Convert class
 * detail::Convert<const char*> c("1337");
 * // use cast operator
 * int i = (int)c;
 * @endcode
 * That's all.
 * @brief Converts from Source type to Target type
 * @tparam Source Source type to convert
 * @param source
 * @return Returned is Convert object
 */
template <typename Source>
inline detail::Convert<Source> convert(Source source) {
    return detail::Convert<Source>(source);
}

template <typename Source, typename Type>
inline detail::Convert<Source> convert(Source source, Type valueOnError) {
    try {
        return detail::Convert<Source>(source);
    }
    catch (BadConvert) {
        return valueOnError;
    }
}

}//namespace uticc

#endif	/* UCONVERT_H */
