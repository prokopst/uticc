#ifndef UNAME_H
#define UNAME_H

#include <typeinfo>

//#ifdef HAVE_CXA_DEMANGLE

// for abi::__cxa_demangle
#include <cxxabi.h>
// for std::malloc, std::free
#include <cstdlib>

namespace uticc {
namespace detail {

/**
 * @tparam Return return type
 * @tparam Size initial size of the buffer
 */
template <typename Return, size_t Size>
inline Return _uname(const std::type_info& info) {
    size_t size = Size;
    // demangle is using realloc
    char* buffer = (char*)std::malloc(Size);
    int status;
    abi::__cxa_demangle(info.name(), buffer, &size, &status);
    // create result value
    Return result = buffer;
    // avoid memory leak
    std::free(buffer);
    return result;
}

}
}

/**
 * @param type Like in typeid, parameter can be a type or a variable
 * @return std::string with demangled name
 */
#define uname(type) uticc::detail::_uname<std::string,127>(typeid(type))

//#else
//
//#define uname(type) typeid(type).name()
//
//#endif


#endif // UNAME_H
