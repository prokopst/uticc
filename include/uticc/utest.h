/**
 * @file utest.h
 * @author Stanislav Prokop
 * @note zlib License / Simplified BSD License
 */

#ifndef utest_H
#define utest_H

#include <iostream>
#include <iomanip>
#include <typeinfo>
#include <map>
#include <string>
#include <vector>
#include <stdexcept>

namespace uticc {

struct AssertionFailure {
    int line;
    const char* file;
    const char* stringifiedLine;
    const char* message;
    
    AssertionFailure(int line, const char* file, const char* stringifiedLine, const char* message=NULL)
    : line(line), file(file), stringifiedLine(stringifiedLine), message(message) {
    }
};

#define uassert(what) \
if ((what) == false) \
    throw uticc::AssertionFailure(__LINE__, __FILE__, "uassert(" #what ")")

#define uassertMessage(what, message) \
if (what == false) \
    throw uticc::AssertionFailure(__LINE__, __FILE__, "uassertMessage(" #what ")", message)

/**
 * @todo Create better interface
 */
class ReportPrinter {
public:
    virtual void beginTestCase(const std::string& name, size_t testsCount) = 0;
    virtual void endTestCase(size_t passed, size_t failed) = 0;
    virtual void test(const std::string& name, bool status, const char* file=NULL, int line=0, const char* stringifiedLine=NULL, const char* message=NULL) = 0;
    virtual void begin(size_t testSuitesCount, size_t testsCount) = 0;
    virtual void end(size_t passed, size_t failed) = 0;
};

class YAMLReportPrinter : public ReportPrinter {
public:
    YAMLReportPrinter(std::ostream& out, bool onlyFailed=true)
     : out(out), onlyFailed(onlyFailed), count(0), failed(0) {
    }
    
    YAMLReportPrinter() : out(std::clog) {    
    }
    
    virtual void beginTestCase(const std::string& name, size_t testsCount) {
        printTabs(1);
        out << "- test suite:" << std::endl;
        printTabs(2);
        out << "name:\t" << name << std::endl;
        printTabs(2);
        out << "count:\t" << testsCount << std::endl;
        printTabs(2);
        out << "tests:" << std::endl;
    }
    
    virtual void endTestCase(size_t passed, size_t failed) {
        
    }

    virtual void test(const std::string& name, bool status, const char* file=NULL, int line=0, const char* stringifiedLine=NULL, const char* message="") {
        ++count;
        if (status == false) {
            ++failed;
        }
        if (this->onlyFailed == false || status == false) {
            printTabs(3);
            out << "- name:\t" << name << std::endl;
            printTabs(3);
            // ternar operator
            out << "  status:\t" << ((status == true) ? "passed" : "failed") << std::endl;
            if (status == false) {
                printTabs(3);
                out << "  file:\t" << file << ':' << line << std::endl;
                printTabs(3);
                out << "  line:\t" << stringifiedLine << std::endl;
                if (message != NULL) {
                    printTabs(3);
                    out << "  message:\t" << message << std::endl;
                }
            }
        }
    }
    
    virtual void begin(size_t testSuitesCount, size_t testsCount) {
        out << "tests count:\t" << testsCount << std::endl;
        out << "test suites:" << std::endl;
    }
    virtual void end(size_t passed, size_t failed) {
        out << "passed:\t" << (this->count - this->failed) << std::endl;
        out << "failed:\t" << this->failed << std::endl;
    }
protected:
    void printTabs(size_t width) {
        out << std::setfill('\t') << std::setw(width) << "";
    }
    std::ostream& out;
    bool onlyFailed;
    size_t count;
    size_t failed;
};

///@cond DETAIL
namespace detail {
class TestCaseBase {
public:
    virtual void setUp() {
    }
    
    virtual void tearDown() {
    }
    
    virtual ~TestCaseBase() {
    }
    
    const std::string& getTestCaseName() const {
        return this->testCaseName;
    }
protected:
    TestCaseBase() {
    }
    
    TestCaseBase(const std::string& testCaseName) : testCaseName(testCaseName) {
    }
    
    std::string testCaseName;
};

/**
 * 
 */
class TestCaseRegistratorBase {
public:
    /**
     * For testing reasons only.
     * @return 
     */
    virtual const std::type_info& typeInfo() const = 0;
    virtual int runTests(ReportPrinter& reportPrinter) = 0;
};

class TypeInfoComparator {
public:
    bool operator()(const std::type_info* first, const std::type_info* second) const {
        return first->before(*second);
    }
};

/**
 * @tparam DUMMY Dummy parameter to force linker to take care of singleton 
 */
template <int DUMMY>
class TTestSuite {
public:
    TTestSuite() : allTestsCount(0) {
    }
    
    /**
     * 
     * @return 
     */
    static TTestSuite* getInstance() {
        if (instance == NULL) {
            instance = new TTestSuite();
        }
        return instance;
    }
    
    /**
     * 
     * @param testSuiteRegistrator
     */
    template <typename SpecificTestSuiteRegistrator>
    void addRegistrator(SpecificTestSuiteRegistrator* testSuiteRegistrator) {
        // explicit cast
        TestCaseRegistratorBase* registrator = static_cast<TestCaseRegistratorBase*>(testSuiteRegistrator);
        this->registrators.push_back(registrator);
    }
    
    /**
     * 
     * @return 
     */
    int run(ReportPrinter& reportPrinter) {
        int failedTestsCount = 0;
        reportPrinter.begin(registrators.size(), 0);
        typename TestCaseRegistrators::iterator iterator = registrators.begin();
        while (iterator != registrators.end()) {
            TestCaseRegistratorBase* registrator = *iterator;
            failedTestsCount += registrator->runTests(reportPrinter);
            ++iterator;
        }
        reportPrinter.end(0, 0);
        return failedTestsCount;
    }
    
    ~TTestSuite() {
        typename TestCaseRegistrators::iterator iterator = registrators.begin();
        while (iterator != registrators.end()) {
            TestCaseRegistratorBase* registrator = *iterator;
            delete registrator;
            ++iterator;
        }
    }

    /**
     */
    typedef std::vector<TestCaseRegistratorBase*> TestCaseRegistrators;
    
    /**
     */
    TestCaseRegistrators registrators;
    
    /**
     */
    static TTestSuite* instance;
    size_t allTestsCount;
private:
    struct AutoPtr {
        AutoPtr() {
            std::cout << "deleter" << std::endl;
        }

        ~AutoPtr() {
            std::cout << "deleter" << std::endl;
            delete instance;
        }
    };
    static AutoPtr deleter;
};

template <int DUMMY>
TTestSuite<DUMMY>* TTestSuite<DUMMY>::instance = NULL;

template <int DUMMY>
typename TTestSuite<DUMMY>::AutoPtr TTestSuite<DUMMY>::deleter;

typedef TTestSuite<0> TestSuite;

/**
 * 
 */
template <typename SpecificTestCase>
class TestCaseRegistrator : public TestCaseRegistratorBase {
public:
    /// Pointer to test method.
    typedef void (SpecificTestCase::*Method)();
    /// Map of methods, key is a string.
    typedef std::map<std::string, Method> Methods;    
    
    TestCaseRegistrator() {
        TestSuite* singleton = TestSuite::getInstance();
        // self registration to singleton
        singleton->addRegistrator(this);
    }
    
    virtual int runTests(ReportPrinter& reportPrinter) {
        SpecificTestCase testCase;
        int failedTestsCount = 0;
        typename Methods::iterator iterator = methods.begin();
        reportPrinter.beginTestCase(testCase.getTestCaseName(), methods.size());
        while (iterator != methods.end()) {
            const std::string& name = iterator->first;
            Method method = iterator->second;
            // try test method
            try {
                testCase.setUp();
                (testCase.*method)();
                testCase.tearDown();
                reportPrinter.test(name, true, NULL);
            }
            catch (AssertionFailure exception) {
                reportPrinter.test(name, false, exception.file, exception.line, exception.stringifiedLine, exception.message);
            }
            // catch anything
            catch (...) {
                failedTestsCount += 1;
                reportPrinter.test(name, false, NULL);
            }
            
            ++iterator;
        }
        reportPrinter.endTestCase(methods.size() - failedTestsCount, failedTestsCount);
        return failedTestsCount;
    }
    
    virtual const std::type_info& typeInfo() const {
        return typeid(SpecificTestCase);
    }
    
    void registerTestMethod(const std::string& name, Method method) {
        this->methods[name] = method;
    }
protected:
    /// Instance of Methods
    Methods methods;
};

}// namespace detail
///@endcond

inline int testRun(ReportPrinter& reportPrinter) {
    detail::TestSuite* testSuite =  detail::TestSuite::getInstance();
    return testSuite->run(reportPrinter);
}

inline int testRun() {
    YAMLReportPrinter yamlReportPrinter;
    detail::TestSuite* testSuite =  detail::TestSuite::getInstance();
    return testSuite->run(yamlReportPrinter);
}

/**
 * @tparam SpecificTestSuite Name of your test case
 */
template <typename SpecificTestSuite>
class TestCase : public detail::TestCaseBase {
protected:
    /**
     * Construct test case with name of class.
     * @todo Demangle (undecorate) name returned by type_info
     */
    TestCase() {
        testCaseName = typeid(SpecificTestSuite).name();
    }
    
    /**
     * Construct test case with different name.
     * @param name Test case name
     */
    TestCase(const std::string& name) : TestCaseBase(name) {
    }
    typedef SpecificTestSuite __u_ChildType;
    /**
     * For registering test suite.
     */
    static detail::TestCaseRegistrator<SpecificTestSuite>* testSuiteRegistrator;
};

///@cond DETAIL
template <typename SpecificTestSuite>
detail::TestCaseRegistrator<SpecificTestSuite>* TestCase<SpecificTestSuite>::testSuiteRegistrator = new detail::TestCaseRegistrator<SpecificTestSuite>();
///@endcond

/**
 * Creates test method self registration. This macro creates instance variable
 * to register method after creation of test case.
 * 
 * For example code
 * @code
 * utest(testName) {
 *     uassert(123 == 123);
 * }
 * @endcode
 * is expanded to following:
 * @code
 * struct __U_testNameRegistrator {
 *     testSuiteRegistrator->registerTestMethod( "testName" , &__u_ChildType :: testName );\
 * };
 * __U_testNameRegistrator __u_testNameRegistrator;
 * void testName () {
 *     uassert(123 == 123);
 * }
 * @endcode
 * @param testName
 */
#define utest(testName) \
struct __U_##testName##Registrator {\
__U_##testName##Registrator() {\
testSuiteRegistrator->registerTestMethod( #testName , &__u_ChildType :: testName );\
}\
};\
__U_##testName##Registrator __u_##testName##Registrator;\
void testName ()

}

#endif
