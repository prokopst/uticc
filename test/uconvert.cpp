#include <uticc/uconvert.h>
#include <uticc/utest.h>
#include <sstream>
#include <string>
#include <climits>

using namespace uticc;

template <typename Number>
std::string getStringFromNumber(Number number, char lastDigitChange) {
    std::ostringstream stream;
    stream << number;
    std::string string = stream.str();
    string[string.length() - 1] += lastDigitChange;
    return string;
}

struct ConversionTestCase : public TestCase<ConversionTestCase> {
    utest(convertToInt) {
        try {
            int i = convert("1337");
            uassert(i == 1337);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
    
    utest(convertToShort) {
        try {
            short i = convert("1337");
            uassert(i == 1337);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
    
    utest(convertToShortFailure) {
        short i = 1338;
        try {
            i = convert("fail");
            uassert(false);
        }
        catch (BadConvert) {
            uassert(i == 1338);
        }
    }
    
    utest(convertToShortOverflow) {
        short i = 0;
        std::string string = getStringFromNumber(SHRT_MAX, 1);
        try {
            i = convert(string);
            uassert(false);
        }
        catch (BadConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertToShortUnderflow) {
        short i = 0;
        std::string string = getStringFromNumber(SHRT_MIN, 1);
        try {
            i = convert(string);
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertToUnsignedShort) {
        try {
            unsigned short i = convert("1337");
            uassert(i == 1337);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
    
    utest(convertToUnsignedShortFailure) {
        unsigned short i = 1338;
        try {
            i = convert("fail");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 1338);
        }
    }
    
    utest(convertToUnsignedShortOverflow) {
        unsigned short i = 0;
        std::string string = getStringFromNumber(USHRT_MAX, 1);
        try {
            i = convert(string);
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertFromNegativeToUnsignedShortFail) {
        unsigned short i = 0;
        try {
            i = convert("-1");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertToLong) {
        try {
            long i = convert("1337");
            uassert(i == 1337);
        }
        catch (BadConvert) {
            uassert(false);
        }
    }
    
    utest(convertToLongFailure) {
        long i = 0;
        try {
            i = convert("failure");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertToLongOverflow) {
        long i = 0;
        std::string string = getStringFromNumber(LONG_MAX, 1);
        try {
            i = convert(string);
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertToLongUnderflow) {
        long i = 0;
        std::string string = getStringFromNumber(LONG_MIN, 1);
        try {
            i = convert(string);
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertToIntFailure) {
        int i = 1338;
        try {
            i = convert("fail");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 1338);
        }
    }
    
    utest(convertToFloat) {
        try {
            float f = convert("13.37");
            uassert(f == 13.37f);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
    
    utest(convertToFloatFailure) {
        float f = 13.37f;
        try {
            f = convert("fail");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(f == 13.37f);
        }
    }
    
    utest(convertToDouble) {
        try {
            double d = convert("13.333337");
            uassert(d == 13.333337);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
    
    utest(convertToDoubleFailure) {
        double d = 0;
        try {
            d = convert("fail");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(d == 0);
        }
    }
    
    utest(convertToUnsignedInt) {
        try {
            unsigned int ui = convert("1337");
            uassert(ui == 1337);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
    
    utest(convertFromNegativeToUnsignedIntFail) {
        unsigned int ui = 0;
        try {
            ui = convert("-1");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(ui == 0);
        }
    }
    
    utest(convertFromStringSpaceValueFail) {
        int i = 0;
        try {
            i = convert("apes 12");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertFromValueSpaceStringFail) {
        int i = 0;
        try {
            i = convert("12 apes");
            uassert(false);
        }
        catch (BadConvert badConvert) {
            uassert(i == 0);
        }
    }
    
    utest(convertFromHexToInteger) {
        int i = 0;
        try {
            i = convert("0x10");
            uassert(i == 16);
        }
        catch (BadConvert badConvert) {
            uassert(false);
        }
    }
};
