#include <uticc/utest.h>
#include <uticc/uname.h>
#include <string>

class ClassName {};

namespace namespaceName {

class ClassName {};

}

namespace firstName {
namespace secondName {

class ClassName {};

}
}

class UNameTestCase : public uticc::TestCase<UNameTestCase> {
public:
    utest(className) {
        std::string name = uname(ClassName);
        uassert(name == "ClassName");
    }
    
    utest(classNameWithNamespace) {
        std::string name = uname(namespaceName::ClassName);
        uassert(name == "namespaceName::ClassName");
    }
    
    
    utest(classNameWithTwoNamespaces) {
        std::string name = uname(firstName::secondName::ClassName);
        uassert(name == "firstName::secondName::ClassName");
    }
};
