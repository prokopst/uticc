#include <uticc/utest.h>
#include <typeinfo>
#include <iostream>

int main(int, char**) {
    uticc::YAMLReportPrinter printer(std::cout, false);
    return uticc::testRun(printer);
}

